package com.universite.universite.web.controller;

import com.universite.universite.model.OrganismeModel;
import com.universite.universite.repository.OrganismeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class OrganismeController {
    @Autowired
    OrganismeRepository repository;

    @GetMapping("/organismes")
    public List<OrganismeModel> getAllAgents() {
        System.out.println("Get all Organismes...");
        List<OrganismeModel> Organismes = new ArrayList<>();
        repository.findAll().forEach(Organismes::add);
        return Organismes;
    }
}
