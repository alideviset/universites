package com.universite.universite.repository;

import com.universite.universite.model.OrganismeModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrganismeRepository extends JpaRepository<OrganismeModel, Long> {

}
