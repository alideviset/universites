package com.universite.universite.model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@Table(name = "organisme")
public class OrganismeModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String libfr;
    private String libar;
    private String logo;

}
